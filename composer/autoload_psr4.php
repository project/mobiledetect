<?php

/*
 * @file
 * autoload_psr4.php
 */

$vendorDir = DRUPAL_ROOT . '/libraries/mobiledetect';

return array(
  'MobileDetect\\' => array($vendorDir . '/src')
);
