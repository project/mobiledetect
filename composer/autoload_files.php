<?php

/*
 * @file
 * autoload_files.php
 */

$vendorDir = DRUPAL_ROOT . '/libraries/mobiledetect';

return array(
  $vendorDir . '/src/MobileDetect.php'
);
