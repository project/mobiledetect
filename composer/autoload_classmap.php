<?php

/*
 * @file
 * autoload_classmap.php
 */

$vendorDir = DRUPAL_ROOT . '/libraries/mobiledetect';

return array(
  'MobileDetect' => $vendorDir . '/src/MobileDetect.php'
);
