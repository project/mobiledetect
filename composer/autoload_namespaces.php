<?php

/*
 * @file
 * autoload_namespaces.php
 */

$vendorDir = DRUPAL_ROOT . '/libraries/mobiledetect';

return array(
  'MobileDetect\\' => array($vendorDir . '/src')
);
